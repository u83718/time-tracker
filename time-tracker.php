<?php

require_once 'db.php';


switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return getTasks();                      // Get today tasks (date)

    case 'POST':
        return saveTask('php://input');         // Save (received tasks)

    default:
        exit(':)');
}

function getTasks() {
    if (!isset($_GET['date']) || !$_GET['date'])
        return;

    if (($tasks = TimeTrackDB::getTasks($_GET['date'])) !== false)
        echo json_encode($tasks);
}

function saveTask($json_data) {
    if (!$task = json_decode(file_get_contents($json_data)))
        exit('Wrong input data');

    echo TimeTrackDB::saveTask($task) === true ? json_encode('ok') : json_encode('ko');
}
