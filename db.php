<?php

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'time_tracker');

class TimeTrackDB {

    private static $inst;
    private $db;
    private $connected;

    private function __construct($host = DB_HOST, $user = DB_USER, $pass = DB_PASS, $name = DB_NAME) {
        $this->db = new mysqli($host, $user, $pass, $name);
        $this->db->set_charset('utf8mb4');

        $this->connected = $this->db->connect_errno === 0;
    }

    private static function initDB() {
        if (!self::$inst instanceof self)
            self::$inst = new self();
    }

    public static function getTasks($date) {
        self::initDB();

        if (!self::$inst->connected)
            return false;

        $select = self::$inst->db->prepare('SELECT * FROM `tt_task` WHERE `started_at` LIKE ?');
        $date = '%' . $date . '%';
        $select->bind_param('s', $date);

        if (!$select->execute()) {
            $select->close();
            return false;
        }

        $res = $select->get_result();
        $tasks = [];
        while ($task = $res->fetch_object())
            $tasks[] = $task;

        $select->close();
        return $tasks;
    }

    public static function saveTask($task) {
        self::initDB();

        if (!self::$inst->connected)
            return false;

        $insert = self::$inst->db->prepare('INSERT INTO `tt_task` VALUES (NULL, ?, ?, ?, ?)');
        $insert->bind_param('siss', $task->name, $task->elapsed, $task->started_at, $task->stopped_at);
        $res = $insert->execute();

        $insert->close();
        return $res;
    }

}
