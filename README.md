## Time Tracker

Here is the source code of the proposed "Time Tracker" exercise by DegustaBox.
The live deployment is available at [time-tracker.ddns.net](http://time-tracker.ddns.net)

### Requeriments
- PHP 5.6+
- MySQL 4+

### Installation
- Windows [XAMPP](https://www.apachefriends.org/es/download.html)

	1) Edit "host" file at: C:\Windows\System32\drivers\etc

	- Add: 127.0.0.1 time-tracker.com

	2) Setup VirtualHost in Apache

	Edit "httpd-vhosts.conf" at: C:\xampp\apache\conf\extra

	- Add replacing YOUR_CUSTOM_PATH correctly:

	```
	<VirtualHost 127.0.0.1:80>
		DocumentRoot "YOUR_CUSTOM_PATH/time-tracker"
		ServerName time-tracker.com
		ErrorLog ${APACHE_LOG_DIR}/time-tracker.com-error.log
		CustomLog ${APACHE_LOG_DIR}/time-tracker.com-access.log combined
		<Directory "YOUR_CUSTOM_PATH/time-tracker">
			AllowOverride All
			Require all granted
		</Directory>
	</VirtualHost>
	```

	3) Start Apache and MySQL modules from XAMPP Control Panel.

	4) Import "time-tracker-structure.sql" file into MySQL. It will create the Database and table structures.

	5) Test if it works.

----

- Linux/Debian
	1) Install LAMP

	> sudo apt install apache2 php7.0 mariadb-server phpmyadmin
	>
	> sudo mysql_secure_installation

	2) Add root password into MariaDB:

	> sudo mariadb -u root
	>
	>> GRANT ALL PRIVILEGES ON *.* TO root@'localhost' IDENTIFIED BY 'YOUR-ROOT-PASSWORD';
	>> FLUSH PRIVILEGES;
	>> EXIT;

	3) Edit hosts file: sudo nano /etc/hosts

	```
	127.0.0.1	time-tracker.com
	```

	4) Setup VirtualHost in Apache

	> sudo mkdir -p /var/www/time-tracker.com
	>
	> sudo nano /etc/apache2/sites-available/time-tracker.com.conf

	```
	<VirtualHost 127.0.0.1:80>
		DocumentRoot /var/www/time-tracker.com
		ServerName time-tracker.com
		ErrorLog ${APACHE_LOG_DIR}/time-tracker.com-error.log
		CustomLog ${APACHE_LOG_DIR}/time-tracker.com-access.log combined
		<Directory /var/www/time-tracker.com>
			AllowOverride All
			Require all granted
		</Directory>
	</VirtualHost>
	```

	> sudo a2ensite time-tracker.com
	>
	> sudo ln -s /etc/apache2/sites-available/time-tracker.com.conf /etc/apache2/sites-enabled/
	>
	> sudo systemctl restart apache2

	5) Import "time-tracker-structure.sql" file into MySQL. It will create the Database and table structures.

	6) Test if it works.

#### If phpMyAdmin is not found (http://localhost/phpmyadmin)
> sudo gedit /etc/apache2/apache2.conf

- Add this include at the end:
```
Include /etc/phpmyadmin/apache.conf
```

> sudo service apache2 restart