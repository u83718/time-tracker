CREATE DATABASE IF NOT EXISTS `time_tracker`
DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE `time_tracker`;

DROP TABLE IF EXISTS `tt_task`;
CREATE TABLE IF NOT EXISTS `tt_task` (
    `id`            int(11)         PRIMARY KEY AUTO_INCREMENT  COMMENT 'Task ID',
    `name`          varchar(95)     NOT NULL                    COMMENT 'Task Name',
    `elapsed`       int(11)         NOT NULL                    COMMENT 'Elapsed (milisec)',
    `started_at`    datetime        NOT NULL                    COMMENT 'Started at',
    `stopped_at`    datetime        NOT NULL                    COMMENT 'Stopped at'
) ENGINE=InnoDB     DEFAULT CHARSET=utf8mb4                     COMMENT='Tasks done';