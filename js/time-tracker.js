/*!
 * Time Tracker JS
 */
'use strict';

//UI config
let timer_input = '#task_manager div.container:first-child input[type="text"]';
let start_btn = '#task_manager div.container:first-child button';
let name_span = '#task_manager div.container:last-child span';
let elapsed_label = '#task_manager div.container:last-child label';
let stop_btn = '#task_manager div.container:last-child button';

let summary_table = '#summary table';

const H_FACTOR = 1000 * 60 * 60;
const M_FACTOR = 1000 * 60;
const S_FACTOR = 1000;

var t_tracker = {};

//On web loads
window.addEventListener('load', initTimerTracker);

function initTimerTracker() {
    //UI
    t_tracker.input = document.querySelector(timer_input);
    t_tracker.start_btn = document.querySelector(start_btn);
    t_tracker.name_span = document.querySelector(name_span);
    t_tracker.elapsed_label = document.querySelector(elapsed_label);
    t_tracker.stop_btn = document.querySelector(stop_btn);

    t_tracker.summary = {};
    t_tracker.summary.table = document.querySelector(summary_table);

    //Internal vars
    t_tracker.task_interval = null;

    t_tracker.task = {};
    t_tracker.task.name = '';
    t_tracker.task.started_at = null;
    t_tracker.task.stopped_at = null;
    t_tracker.task.elapsed = 0;

    t_tracker.summary.date = getLocalDate();
    t_tracker.summary.elapsed = 0;
    t_tracker.summary.tasks = [];
    t_tracker.summary.tasks_by_name = [];

    //Init Summary
    getTasks(getLocalDate().toISOString().slice(0, 10));
    printSummary();

    //Events
    t_tracker.input.addEventListener('keyup', inputProcess);
    t_tracker.start_btn.addEventListener('click', startTimer);
    t_tracker.stop_btn.addEventListener('click', stopTimer);

    t_tracker.stop_btn.disabled = true;
}

function inputProcess(e) {
    if (e.keyCode === 13)
        return startTimer();

    if (this.value.length === 0)
        return;

    t_tracker.input.style = '';
    this.value = this.value[0].toUpperCase() + this.value.substr(1);
}

function startTimer() {
    t_tracker.task = {};

    t_tracker.task.name = t_tracker.input.value.trim();
    if (t_tracker.task.name.length === 0) {
        t_tracker.input.style = 'border: 1px solid red';
        t_tracker.input.focus();

        return;
    }

    t_tracker.input.disabled = true;
    t_tracker.start_btn.disabled = true;
    t_tracker.stop_btn.disabled = false;

    t_tracker.name_span.textContent = t_tracker.task.name;
    t_tracker.input.value = '';


    t_tracker.task.started_at = getLocalDate();
    startTimeTracker();
    t_tracker.task_interval = setInterval(startTimeTracker, 1000);
}

function stopTimer() {
    clearInterval(t_tracker.task_interval);

    saveTask(t_tracker.task);                   //Intended Synchronous Request
    //if OK... continue

    addTaskToSummary(t_tracker.task);

    t_tracker.name_span.textContent = '';
    t_tracker.elapsed_label.textContent = '0:00:00';

    t_tracker.input.disabled = false;
    t_tracker.start_btn.disabled = false;
    t_tracker.stop_btn.disabled = true;
}

function startTimeTracker() {
    t_tracker.task.stopped_at = getLocalDate();
    t_tracker.task.elapsed = t_tracker.task.stopped_at - t_tracker.task.started_at;

    splitTimeParts(t_tracker.task);
    t_tracker.elapsed_label.textContent = t_tracker.task.h + ':' + t_tracker.task.m + ':' + t_tracker.task.s;
}

function splitTimeParts(task) {
    task.h = String(Math.floor((task.elapsed / H_FACTOR)));
    task.m = addZero(Math.floor((task.elapsed / M_FACTOR) % 60));
    task.s = addZero(Math.floor((task.elapsed / S_FACTOR) % 60));
}
function addZero(i) {
    return i < 10 ? '0' + i : String(i);
}

/*
 * Task Summary
 */
function addTaskToSummary(task) {
    t_tracker.summary.tasks.push(task);

    clearSummary();
    printSummary();
}

function printSummary() {
    for (let i in t_tracker.summary.tasks) {                                        //Generate tasks by name increasing the time
        let task = t_tracker.summary.tasks[i];

        if (!t_tracker.summary.tasks_by_name[task.name])                            //If not exists, make a copy
            t_tracker.summary.tasks_by_name[task.name] = Object.assign({}, task);
        else                                                                        //else, increase the task time
            t_tracker.summary.tasks_by_name[task.name].elapsed += task.elapsed;

        splitTimeParts(t_tracker.summary.tasks_by_name[task.name]);
    }

    for (let name in t_tracker.summary.tasks_by_name)
        printTask(t_tracker.summary.tasks_by_name[name]);

    updateTotalSummary();
}

function printTask(task) {
    let row = t_tracker.summary.table.insertRow(t_tracker.summary.table.rows.length - 1);

    row.insertCell(0).innerHTML = task.name;
    row.insertCell(1).innerHTML = task.h + 'h ' + task.m + 'min ' + task.s + 'sec';

    t_tracker.summary.elapsed += task.elapsed;
}

function updateTotalSummary() {
    splitTimeParts(t_tracker.summary);

    let total = t_tracker.summary.table.rows[t_tracker.summary.table.rows.length - 1];
    let today = t_tracker.summary.date;

    total.cells[0].innerHTML = 'TOTAL ' + today.getDate() + '.' + (today.getMonth() + 1) + '.' + today.getFullYear();
    total.cells[1].innerHTML = t_tracker.summary.h + 'h ' + t_tracker.summary.m + 'min ' + t_tracker.summary.s + 'sec';
}

function clearSummary() {
    let len = t_tracker.summary.table.rows.length - 2;
    if (len === 0)
        return;

    for (let i = 0; i < len; ++i)
        t_tracker.summary.table.deleteRow(1);

    t_tracker.summary.tasks_by_name = [];
    t_tracker.summary.elapsed = 0;
    updateTotalSummary();
}

/*
 * DB persistence
 */
function getTasks(date) {
    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'time-tracker.php?date=' + date, false);
    xhr.send();

    t_tracker.summary.tasks = JSON.parse(xhr.responseText);
}

function saveTask(task) {
    let xhr = new XMLHttpRequest();

    xhr.open('POST', 'time-tracker.php', false);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.send(JSON.stringify(task));

    if (JSON.parse(xhr.responseText) === 'ok')
        console.log('Debug: Task added');
}

/**
 * Get the local datetime
 *
 * @returns {Object} Date object
 */
function getLocalDate() {
    let tzoffset = (new Date()).getTimezoneOffset() * 60000;    //https://stackoverflow.com/questions/10830357/javascript-toisostring-ignores-timezone-offset

    return new Date(Date.now() - tzoffset);
}

/*
 * IE Polyfill for Object.assign
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
 */
if (typeof Object.assign !== 'function') {      // Must be writable: true, enumerable: false, configurable: true
    Object.defineProperty(Object, "assign", {
        value: function assign(target, varArgs) {                                       // .length of function is 2
            'use strict';
            if (target === null || target === undefined)
                throw new TypeError('Cannot convert undefined or null to object');

            var to = Object(target);

            for (var index = 1; index < arguments.length; index++) {
                var nextSource = arguments[index];

                if (nextSource !== null && nextSource !== undefined)
                    for (var nextKey in nextSource)
                        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey))  // Avoid bugs when hasOwnProperty is shadowed
                            to[nextKey] = nextSource[nextKey];
            }
            return to;
        },
        writable: true,
        configurable: true
    });
}